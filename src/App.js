import React, { Component } from 'react';
import {connect} from 'react-redux';
import './App.css';

class App extends Component {
  render() {
    const buttons= ['0','1','2','3','4','5','6','7','8','9'];
    return (
        <div className="App">
          <div className="Veiw">
            <div className={this.props.isRight ? 'green' : null}>{this.props.ctr}</div>
          </div>
            {buttons.map(btn =>{
              return(
                  <button key={btn} className="btn" onClick={()=>this.props.putNumber(btn)}>{btn}</button>
              )
            })}
            <button  className="del" onClick={this.props.delete}>Delete</button>
          <button className="enter" onClick={this.props.check} >Enter</button>
        </div>
    );


  }
}

const mapStateToProps = state=>{
    return{
        ctr: state.stars,
        isRight: state.isRight,
    };
};

const mapDispatchToProps = dispatch =>{
    return{
        putNumber: (value) => dispatch({type: 'PUT_NUMBER', value: value}),
        check: () => dispatch({type: 'CHECK'}),
        delete: () => dispatch({type: 'DELETE'}),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
