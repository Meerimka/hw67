
const initialState={
    code: '',
    trueCode: '1234',
    stars: '',
    isRight: false,
};

const reducer = (state= initialState, action) =>{
 if(action.type ==='PUT_NUMBER'){
     if(state.code.length < 4) {
         return{
             ...state,
             code: state.code +=action.value,
             stars: state.stars +='*',
         };
     }else{
         alert('You can enter only 4 symbols!')
     }
 }
 if(action.type ==='CHECK'){
     if(state.code === state.trueCode){
         alert('Access Granted!');
         return{
             ...state,
             isRight: true,
         }
     } else {
         alert('Not correct!!')
     }
 }
    if(action.type ==='DELETE'){
        const newStars = state.stars.substr(0, state.stars.length -1)
        const newCode = state.code.substr(0, state.code.length -1)
        return{
            ...state,
            stars: newStars,
            code:newCode,
            isRight: false
        };
    }

    return state;
}

export default reducer;